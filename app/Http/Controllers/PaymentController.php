<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Cart;
use Stripe;
use Session;

class PaymentController extends Controller
{

	public function index()
	{
		$carts = Cart::where('user_id',Session::get('id'))->with('product')->get();
		$price = 0;
		foreach ($carts as $cart) {
			$price += $cart->product->price * $cart->count;
		}
		if ($price > 0) {
			return view('pay')->with('totalprice' , $price);
		}
		else{
			return view('pay')->with('error', 'Your cart is empty. Nothing to pay!')->with('totalprice' , $price);
		}
        // return view('pay');
	}

	public function makePayment(Request $request)
	{
		$carts = Cart::where('user_id',Session::get('id'))->get();
		$price = 0;
		foreach ($carts as $cart) {
			$price += $cart->product->price * $cart->count;
		}
		Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		Stripe\Charge::create ([
			"amount" => $price * 100,
			"currency" => "inr",
			"source" => $request->stripeToken,
			"description" => "Make payment and chill." 
		]);
		$order = new Order;
		$order->user_id = Session::get("id");
		$order->total = $price;
		$order->save();	

		foreach ($carts as $product) {
			$order_details = new OrderDetail;
			$order_details->order_id = $order->id;
			$order_details->product_id = $product->product_id;
			$order_details->count = $product->count;
			$order_details->save();
		}

		$cart = [];
        Cart::where("user_id",Session::get("id"))->delete();


		Session::flash('success', 'Payment successfully made.');

		return back();
	}
}