<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Validator;
use Hash;
use Redirect;
use App\Models\Users;
use Session;
use Mail;

class UserController extends Controller
{
    public function home() {
        return view("home");
    }
    public function index() {
        $user = ["name" => "Ani", "surname" => "Sargsyan", "age" => 30, "photo" => "https://www.growthengineering.co.uk/wp-content/uploads/2014/06/Interaction-design-user-experience.png"];
        $products = Product::get();
// dd($products);
        return view("index")->with("us1", $user)->with("products", $products);
    }

    public function LoginPage() {
        return view("login");
    }
    public function RegisterPage() {
        return view("register");
    }
    public function Register(Request $r) {
        $validator = Validator::make($r->all(), [
            "name" => "required|min:2|max:20|string",
            "surname" => "required|min:2|max:30|string",
            "age" => "required|gt:18|integer",
            "email" => "required|unique:users,email|email:rfc|string",
            "password" => "required|min:8|max:20|string",
            "confirm_password" => "same:password",
        ]);
// dd($validator->errors());
        if ($validator->fails()) {
            return Redirect::to("/register")->withErrors($validator)->withInput();
        }
        else {
            $user = new Users;
            $user->name = $r->name;
            $user->surname = $r->surname;
            $user->age = $r->age;
            $user->email = $r->email;
            $user->password = Hash::make($r->password);
            $user->photo = "default.png";
            $user->save();
            $hash = md5($user->id.$user->email);
            $data = [
                'name'=>$user->name,
                'surname'=>$user->surname,
                'id'=>$user->id,
                'hash'=>$hash
            ];
            Mail::send("mail", $data, function($message) use($user) {
                $message->to($user->email, "HaTik92")->subject('User Verifikation');
                $message->from('t.hakobyan.92@gmail.com','Tigran');
            });
            Session::flash("success", "You have registered");
            return Redirect::to("/");
        }
    }
    public function Login(Request $r) {
        $validator = Validator::make($r->all(), [
            "email" => "required",
            "password" => "required",
        ]);
        $user = Users::where("email", $r->email)->first();
        $validator->after(function($validator)use($user, $r) {
            if (!$user) {
                $validator->errors()->add("email", "This E-mail is nor registrated");
            }
            elseif (!Hash::check($r->password, $user["password"])) {
                $validator->errors()->add("email", "E-mail or password is incoret");
            }

        });
        if ($validator->fails()) {
            return Redirect::to("/")->withErrors($validator)->withInput();
        }
        else {
            Session::put([
                "id"=>$user["id"],
            ]);
            return Redirect::to("/product/home");
        }


    }
    public function LogOut() {
        Session::flush();
        return Redirect::to("/");
    }

    public function activate($id, $hash){
        $user = Users::where("id",$id)->first();
        if ($user) {
            if ($hash == md5($id.$user->email)) {
                $user->activ = 1;
                $user->save();
                return Redirect::to('/')->with('success','Verification success');
            }
        }
    }
}
