<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use Redirect;
use App\Models\Product;
use Session;
use App\Models\ProductPhoto;
use App\Models\Wishlist;
use App\Models\Cart;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
	function Home() {
		return view("home");
	}
	function Products() {
		$products = Product::where('user_id', '!=', Session::get('id'))->with("photos")->get();
        // dd($products);
		return view("products")->with("products", $products);
	}
	function Addproduct() {
		$cat = Category::get();
		return view("addproduct")->with("cat", $cat);
	}
	function Addnewproduct(Request $r) {
		$validator = Validator::make($r->all(), [
			"name" => "required|min:2|max:20|string",
			"count" => "required|gt:0|integer",
			"price" => "required|integer",
			"description" => "required|min:2|max:100|string",

		]);
    	// dd($validator->errors());
		if ($validator->fails()) {
			return Redirect::to("/product/add/product")->withErrors($validator)->withInput();
		}
		else{
			$product = new Product;
			$product->name = $r->name;
			$product->count = $r->count;
			$product->price = $r->price;
			$product->description = $r->description;
			$product->user_id = Session::get('id');
			$product->category_id = $r->category;
			$product->save();
			if($r->hasfile('photo'))
			{
				foreach($r->file('photo') as $image)
				{
					$name=time().$image->getClientOriginalName();
					$image->move(public_path().'/img/product/', $name);  
					$photo= new ProductPhoto;
					$photo->product_id = $product->id;
					$photo->address = $name;
					$photo->save();
				}
			}
			Session::flash('success', "ok");
			return Redirect::to("product/addproduct");
		}
    	// else {
    	// 	$user = new Users;
    	// 	$user->name = $r->name;
    	// 	$user->surname = $r->surname;
    	// 	$user->age = $r->age;
    	// 	$user->email = $r->email;
    	// 	$user->password = Hash::make($r->password);
     //        $user->photo = "default.png";
    	// 	$user->save();
    	// 	Session::flash("success", "You have registered");
    	// 	return Redirect::to("/register");
    	// }
		return view('addnewproduct');
	}

	function Singleproduct($id) {
		$product = Product::where("id", $id)->with("photos")->first();
        // dd($product);
		$id = Session::get('id');
		return view("singleproduct")->with("product", $product)->with('id',$id);
	}

	function Deleteproduct(Request $r) {
        // dd($r);
		$id = $r->id;
		Product::where("id", $id)->delete();
		return "OK";
	}
	function Wishlist() {
		$wishlist = Wishlist::where("user_id", Session::get('id'))->with("product")->get();
		// $products = 
		for ($i=0; $i < $wishlist->count(); $i++) { 
			$id = $wishlist[$i]['product_id'];
			$data = Product::where('id', $id)->with('photos')->first();
			$photo = $data['photos'][0]['address'];
			$wishlist[$i]['product']['address'] = $photo;
		}
		// dd($wishlist);
		return view("wishlist")->with("wishlist", $wishlist);
	}
	function Cart() {
		$cart = Cart::where("user_id", Session::get('id'))->with("product")->get();
		for ($i=0; $i < $cart->count(); $i++) { 
			$id = $cart[$i]['product_id'];
			$data = Product::where('id', $id)->with('photos')->first();
			$photo = $data['photos'][0]['address'];
			$cart[$i]['product']['address'] = $photo;
		}
		// dd($cart);
		return view("cart")->with("cart", $cart);
	}
	function AddWish(Request $r) {
		$id = $r->id;
		$wishchek = Wishlist::where('user_id', Session::get('id'))->where('product_id', $id)->get();
		$cartchek = Cart::where('user_id', Session::get('id'))->where('product_id', $id)->get();
		if ($wishchek->count() > 0) {
			return "falsewish";
		}
		elseif ($cartchek->count() > 0) {
			return "falsecart";
		}
		else {
			$wish = new Wishlist;
			$wish->user_id = Session::get('id');
			$wish->product_id = $id;
			$wish->save();
			return "true";
		}
	}
	function AddCart(Request $r) {
		$id = $r->id;
		$product_id = $r->product_id;
		$cart = new Cart;
		$cart->user_id = Session::get('id');
		$cart->product_id = $product_id;
		$cart->count++;
		$cart->save();
		WishList::where("id", $id)->delete();
		return "OK";
	}
	function Deletewishlist(Request $r) {
		$id = $r->id;
		WishList::where("id", $id)->delete();
		return "OK";
	} 

	function Deletecart(Request $r) {
		$id = $r->id;
		Cart::where("id", $id)->delete();
		return "OK";
	}

	function checkout () {
		$carts = Cart::where("user_id", Session::get('id'))->with("product")->get();
		$total = 0;
		// $total = DB::table('products')->sum('products.price')->join('products', 'carts.product_id', '=', 'products.id')->where("cart.user_id", Session::get('id'))->select('products.price')->get();
		// $total = Cart::where("user_id", Session::get('id'))->with("product")->sum()
		for ($i=0; $i < $carts->count(); $i++) { 
			$total += $carts[$i]['count'] * $carts[$i]['product']['price'];
		}
		// dd($total);
		return view('checkout')->with('carts', $carts)->with('total', $total);
	}
}
