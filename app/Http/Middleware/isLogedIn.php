<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Redirect;

class isLogedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Session::has('id')) {
            return $next($request);
        }
        else {
            Session::flush();
            return Redirect::to('')->with("message", "You dont have permition to this page");
        }
    }
}
