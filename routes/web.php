<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\isLogedIn;\


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
// Route::get('/oldwelcome', function () {
//     return view('oldwelcome');
// });
// Route::get('/about', function () {
//     return view('about');
// });
// Route::get('/contact', function () {
//     return view('contact');
// });
// Route::get('/index', function () {
//     return view('index');
// });
// Route::get('/', "UserController@home");
// Route::get('/index', "UserController@index");
Route::get('/', "UserController@LoginPage");
Route::get('/register', "UserController@RegisterPage");
Route::post('/registration', "UserController@Register");
Route::post('/login', "UserController@Login");
Route::get('/product/home', "ProductController@Home");
Route::get('/product/allproduct', "ProductController@Products")->middleware("isLogedIn");
Route::get('/product/addproduct', "ProductController@Addproduct");
Route::get('/product/wishlist', "ProductController@Wishlist");
Route::get('/product/cart', "ProductController@cart");
Route::get('/product/checkout', "ProductController@checkout");
Route::post('/product/add/product', "ProductController@Addnewproduct");
Route::get('/product/single/{id}', "ProductController@Singleproduct");
Route::post('/product/single/delete', "ProductController@Deleteproduct");
Route::post('/product/wishlist/delete', "ProductController@Deletewishlist");
Route::post('/product/cart/delete', "ProductController@Deletecart");
Route::post('/product/allproduct/addwish', "ProductController@AddWish");
Route::post('/product/allproduct/add-cart', "ProductController@AddCart");
Route::get('/logout', "UserController@LogOut");
Route::get('/user/activate/{id}/{hash}',"UserController@activate");


Route::get('/pay/order', 'PaymentController@index');
Route::post('/transaction', 'PaymentController@makePayment')->name('make-payment');
// Route::group()

