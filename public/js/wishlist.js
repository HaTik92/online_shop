$(document).ready(function() {
	const _token = $("#_token").val()
	$(".addwish").click(function(event) {
		event.preventDefault()
		
		let id = $(this).attr("id")
		console.log(id)
		console.log(_token)

		$.ajax({
			url: '/product/allproduct/addwish',
			type: 'POST',
			data: {_token: _token, id:id},
			success: function (r) {
				if (r == "true") {
					swal({
						title: "Good job!",
						icon: "success",
					});
				}
				else if (r == "falsewish") {
					swal({
						title: "This product has already been added in wishlist!",
						icon: "error",
					});

				}
				else {
					swal({
						title: "This product has already been added in cart!",
						icon: "error",
					});

				}
			}
		})



	});
});