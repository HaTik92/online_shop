$(document).ready(function() {
	const _token = $("#_token").val()
	$(".add-cart").click(function(event) {
				
		let id = $(this).attr("data-id")
		let product_id = $(this).attr("data-prod_id")
		console.log(id)
		console.log(_token)

		$.ajax({
			url: '/product/allproduct/add-cart',
			type: 'POST',
			data: {_token: _token, id:id, product_id:product_id},
			success: function (r) {
				console.log(r)
			}
		})



	});
	$(".delete_wishlist").click(function(event) {
				
		let id = $(this).attr("data-id")
		
		$.ajax({
			url: '/product/wishlist/delete',
			type: 'POST',
			data: {_token: _token, id:id},
			success: function (r) {
				console.log(r)

			}
		})
		$(this).parent().remove()



	});


	$(".delete_cart").click(function(event) {
				
		let id = $(this).attr("data-id")
		
		$.ajax({
			url: '/product/cart/delete',
			type: 'POST',
			data: {_token: _token, id:id},
			success: function (r) {
				console.log(r)
				
			}
		})
		$(this).parent().remove()



	});
});