<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<title>Document</title>
	<link rel="stylesheet" href="{{asset('css/register.css')}}">
</head>
<body>
	@if(Session::get("success"))
	<h1 class="alert alert-success">
		{{Session::get("success")}}
	</h1>
	@endif
	<div class="container">
		<form action="{{url('/registration')}}" method="post">
			@csrf
			<label for="name">{{$errors->first("name")}}</label>
			<input type="text" value="{{old('name')}}" name="name" class="form-control" placeholder="Enter name">
			<label for="surname">{{$errors->first("surname")}}</label>
			<input type="text" value="{{old('surname')}}" name="surname" class="form-control" placeholder="Enter surname">
			<label for="age">{{$errors->first("age")}}</label>
			<input type="text" value="{{old('age')}}" name="age" class="form-control" placeholder="Enter age">
			<label for="email">{{$errors->first("email")}}</label>
			<input type="text" value="{{old('email')}}" name="email" class="form-control" placeholder="Enter e-mail">
			<label for="password">{{$errors->first("password")}}</label>
			<input type="password" name="password" class="form-control" placeholder="Enter new password">
			<label for="confirm_password">{{$errors->first("confirm_password")}}</label>
			<input type="password" name="confirm_password" class="form-control" placeholder="Confirm password">
			<button class="btn btn-info">Register</button>
		</form>
	</div>
</body>
</html>