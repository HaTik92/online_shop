@extends('layouts.layout')
@section("title", "Products")
@section("content")
<div class="container">
	<div class="row">
		<div class="col-lg-3 bg-dark p-4 text-light">
			<h1>{{$product["name"]}}</h1>
			<h1>{{$product["count"]}}</h1>
			<h1>{{$product["price"]}}</h1>
			<h1>{{$product["description"]}}</h1>
			@if($product["user_id"] == $id)
				<button class="btn btn-danger" id="delete_product" data-id="{{$product['id']}}">Delete</button>
			@endif
			{{-- <img src="{{asset('img/product/'.$products['photos'][0]['address'])}}" width="200" alt=""> --}}
		</div>
	</div>
</div>
<script src="{{asset('js/product.js')}}"></script>
@endsection