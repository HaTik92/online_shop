@extends('layouts.layout')
@section("title", "Home page")
@section("content")
         <!-- slider start -->
        <div class="slider-wrap">
            <div class="preview-2">
                <div id="nivoslider" class="slides"> 
                     <img src="{{asset('img/slider/slider1.jpg')}}" alt="" title="#slider-direction-1"  />
                     <img src="{{asset('img/slider/slider2.jpg')}}" alt="" title="#slider-direction-2"  />
                     <img src="{{asset('img/slider/slider3.jpg')}}" alt="" title="#slider-direction-2"  />
                </div>
                <!-- direction 1 -->
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="nivo_text">    
                        <div class="slider-content slider-text-1">
                            <div class="wow zoomInUp" data-wow-delay="0.3s">
                                <h3>Shopppie shop</h3>
                            </div>
                        </div>
                        <div class="slider-content slider-text-2">
                            <div class="wow zoomInUp" data-wow-delay=".2s">
                               <h2 class="title">New Season heroes</h2>
                            </div>
                        </div>
                        <div class="slider-content slider-text-4">
                            <div class="wow zoomInUp" data-wow-delay=".3s">
                               <h3 class="title ti7">East Meets West in CWST’s Fall Winter 2015 Lookbook</h3>
                            </div>
                        </div>
                        <div class="slider-content slider-text-3">
                            <div class="wow zoomInUp" data-wow-delay=".4s">
                                <a href='shop-full-grid.html' class='slider-button'>Shop Now</a>
                            </div>
                        </div>
                    </div>    
                </div>
                <!-- direction 2 -->
                <div id="slider-direction-2" class="slider-direction">
                    <div class="nivo_text">  
                        <div class="slider-content slider-text-1">
                            <div class="wow zoomInUp" data-wow-delay="0.3s">
                                <h3>Shopppie shop</h3>
                            </div>
                        </div>
                        <div class="slider-content slider-text-2">
                            <div class="wow zoomInUp" data-wow-delay=".2s">
                                <h2 class="title">New Season heroes</h2>
                            </div>
                        </div>
                        <div class="slider-content slider-text-4">
                            <div class="wow zoomInUp" data-wow-delay=".3s">
                               <h3 class="title ti7">East Meets West in CWST’s Fall Winter 2015 Lookbook</h3>
                            </div>
                        </div>
                        <div class="slider-content slider-text-3">
                            <div class="wow zoomInUp" data-wow-delay=".4s">
                                <a href='shop-full-grid.html' class='slider-button'>Shop Now</a>
                            </div>
                        </div>
                    </div>     
                </div>
            </div>
        </div>
        <!-- slider end -->
        <!-- service start -->
        <div class="service_area clearfix">
            <div class="single-service">
                <div class="service-icon">
                    <img src="{{asset('img/icon/s1.png')}}" alt="">
                </div>
                <div class="service-info">
                    <h2>Free Shipping</h2>
                    <p>Free for all product</p>
                </div>
            </div>
            <div class="single-service">
                <div class="service-icon">
                    <img src="{{asset('img/icon/s2.png')}}" alt="">
                </div>
                <div class="service-info">
                    <h2>Money Guarante</h2>
                    <p>Money back guarantee</p>
                </div>
            </div>
            <div class="single-service hidden-sm">
                <div class="service-icon">
                    <img src="{{asset('img/icon/s3.png')}}" alt="">
                </div>
                <div class="service-info">
                    <h2>Safe Shopping</h2>
                    <p>Enjoy safe shopping</p>
                </div>
            </div>
            <div class="single-service hidden-sm hidden-md">
                <div class="service-icon">
                    <img src="{{asset('img/icon/s4.png')}}" alt="">
                </div>
                <div class="service-info">
                    <h2>Online Support</h2>
                    <p>24/7 we provide online support</p>
                </div>
            </div>
        </div>
        <!-- service end -->
        <!-- banner start -->
        <div class="banner-area clearfix">
            <div class="single-banner left">
                <a href="#" class="border-hover"><img src="{{asset('img/banner/1.jpg')}}" alt=""></a>
                <div class="single-banner-text">
                    <h3><a href="#">60% OFF<br>
                    Woman<br> 
                    Collection</a></h3>
                    <a href="shop-full-grid.html" class="shop-button hidden-sm hidden-xs">shop now</a>
                </div>
            </div>
            <div class="single-banner right">
                <a href="#" class="border-hover"><img src="{{asset('img/banner/2.jpg')}}" alt=""></a>
                <div class="single-banner-text">
                    <h3><a href="#">50%<br>
                    OFF Men<br> 
                    Collection</a></h3>
                    <a href="shop-full-grid.html" class="shop-button hidden-sm hidden-xs">shop now</a>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- featured start -->
        <div class="featured-area clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title">
                            <h2>featured products</h2>
                        </div>
                    </div>
                </div>
                <div class="row">       
                    <div class="col-xs-12">
                        <div class="section-tab">
                            <div class="section-tab-menu text-left">
                                <ul role="tablist">
                                    <li role="presentation" class="active"><a href="#women" aria-controls="women" role="tab" data-toggle="tab">women</a></li>
                                    <li role="presentation"><a href="#men" aria-controls="men" role="tab" data-toggle="tab">Men</a></li>
                                    <li role="presentation"><a href="#trendy" aria-controls="trendy" role="tab" data-toggle="tab">trendy</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="tab-content row">
                                <div id="women" role="tabpanel" class="active section-tab-item">
                                    <div class="tab-item-slider">
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
<!--                                                            <span class="old-price">$26</span>-->
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
<!--                                                            <span class="old-price">$180</span>-->
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
<!--                                                            <span class="old-price">$180</span>-->
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
<!--                                                            <span class="old-price">$180</span>-->
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
<!--                                                            <span class="old-price">$180</span>-->
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
<!--                                                            <span class="old-price">$180</span>-->
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-width">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
<!--                                                            <span class="old-price">$180</span>-->
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="men" role="tabpanel" class="section-tab-item">
                                    <div class="tab-item-slider">
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="trendy" role="tabpanel" class="section-tab-item">
                                    <div class="tab-item-slider">
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item box-shadow-effect">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>     
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        <!-- featured end -->
        <!-- banner timer start -->
        <div class="banner-timer-area clearfix">
            <div class="single-banner-small small-one">
                <a class="border-hover" href="#"><img src="{{asset('img/banner/3.jpg')}}" alt=""></a>
                <div class="timer-banner-text">
                    <h2>latest<br> backpack</h2>
                    <a href="shop-full-grid.html" class="view-more">view more</a>
                </div>
                <p>backpack</p>
            </div>
            <div class="single-banner-big big-one hidden-xs">
                <a class="border-hover" href="#"><img src="{{asset('img/banner/4.jpg')}}" alt=""></a>
                <div class="timer-banner-text">
                    <h2>taratari<br> offer kintu<br> furieee gelo!</h2>
                </div>
                <div class="timer">
                    <div data-countdown="2016/06/01" class="timer-grid"></div>
                </div>
                <p>hot product</p>
            </div>
            <div class="single-banner-small small-two hidden-md hidden-sm">
                <a class="border-hover" href="#"><img src="{{asset('img/banner/5.jpg')}}" alt=""></a>
                <div class="timer-banner-text">
                    <h2>latest<br> sunglass</h2>
                    <a href="shop-full-grid.html" class="view-more">view more</a>
                </div>
                <p>women's sunglass</p>
            </div>
        </div>
        <!-- banner timer end -->
        <!-- popular start -->
        <div class="popular-area">
            <div class="container">
                <div class="row"> 
                    <div class="col-xs-12">
                        <div class="section-title">
                            <h2>popular products</h2>
                        </div>   
                        <div class="trend_tab">
                            <div class="tendy-tab-menu text-right">
                                <ul role="tablist">
                                    <li role="presentation" class="active"><a href="#watches" aria-controls="watches" role="tab" data-toggle="tab">Watches</a></li>
                                    <li role="presentation"><a href="#t-shirt" aria-controls="t-shirt" role="tab" data-toggle="tab">t-shirt</a></li>
                                    <li role="presentation"><a href="#shoes" aria-controls="shoes" role="tab" data-toggle="tab">shoes</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="trend_content row">
                                <div id="watches" role="tabpanel" class="active trend-item">
                                    <div class="trend-item-slider">
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/5.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/6.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/7.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/8.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/9.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/10.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/11.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/12.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/13.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/14.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/5.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div> 
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div> 
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/6.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/7.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/14.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/11.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/8.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/9.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/10.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="t-shirt" role="tabpanel" class="trend-item">
                                    <div class="trend-item-slider">
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div> 
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div> 
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/5.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/6.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/7.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/8.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/13.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/14.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/5.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/6.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/7.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/8.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/13.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/14.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div id="shoes" role="tabpanel" class="trend-item">
                                    <div class="trend-item-slider">
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/14.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div> 
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div> 
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/13.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/12.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/11.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/10.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div> 
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div> 
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/9.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/8.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/7.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/6.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/5.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div> 
                                                        <div class="new-sale">
                                                            <span>new</span>
                                                        </div> 
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/3.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/14.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/13.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">  
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/12.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="single-product">
                                                <div class="single-product-item">
                                                    <div class="single-product-img clearfix hover-effect">
                                                        <a href="#">
                                                            <img class="primary-image" src="{{asset('img/product/11.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="single-product-info clearfix"> 
                                                        <div class="pro-price">
                                                            <span class="new-price">$25.99</span>
                                                        </div>  
                                                    </div>
                                                    <div class="product-content text-center">
                                                        <h3>Full sleev women shirt</h3>
                                                        <h4><a href="#">view details</a></h4>
                                                    </div>
                                                    <div class="product-action">      
                                                        <ul>
                                                            <li><a href="#" data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                            <li class="add-bag"><a href="#" data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                            <li><a href="#" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>   
                                </div>
                            </div>            
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        <!-- popular end -->
        <!-- newsletter start -->
        <div class="newsletter-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title">
                            <h2>newsletter</h2>
                        </div>
                        <div class="newsletter-box text-center">
                            <form action="#">
                                <input type="text" placeholder="Type your E-mail address">
                                <button type="submit">Subscribe Now</button>
                            </form>
                            <div class="subscribing">
                                <label class="checkbox-title">
                                    <input id="transfer" value="bank" name="subscribe" type="checkbox">By Subscribing to our newsletter you agree to receive emails from us. 
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- newsletter end -->
        <!-- latest blog start -->
        <div class="latest-blog-area">
            <div class="container">
                <div class="row">
                   <div class="col-xs-12">
                        <div class="section-title">
                            <h2>latest news</h2>
                        </div>
                   </div>
                </div>
                <div class="row">
                    <div class="latest-blog-slider">
                        <div class="col-md-6 col-xs-12">
                            <div class="single-latest-blog">
                                <div class="single-latest-blog-img">
                                    <a href="blog-details.html">
                                        <img src="{{asset('img/blog/1.jpg')}}" alt="">
                                    </a>
                                </div>
                                <div class="single-latest-blog-text">
                                    <div class="date-comment clearfix">
                                        <h4>Art, Business</h4>
                                        <h5>Jan 18, 2016 - Comments 5</h5>   
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-details.html">Lorem Ipsum Dammy Title</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodte porincidi ut labore et dolore magna aliqua. </p>   
                                    </div>
                                    <div class="continue-reading">
                                        <a href="blog-details.html">Continue Reading</a>
                                        <div class="blog-icon">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                            </ul>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="single-latest-blog">
                                <div class="single-latest-blog-img">
                                    <a href="blog-details.html">
                                        <img src="{{asset('img/blog/2.jpg')}}" alt="">
                                    </a>
                                </div>
                                <div class="single-latest-blog-text">
                                    <div class="date-comment clearfix">
                                        <h4>Art, Business</h4>
                                        <h5>Jan 18, 2016 - Comments 5</h5>   
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-details.html">Lorem Ipsum Dammy Title</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodte porincidi ut labore et dolore magna aliqua. </p>   
                                    </div>
                                    <div class="continue-reading">
                                        <a href="blog-details.html">Continue Reading</a>
                                        <div class="blog-icon">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                            </ul>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="single-latest-blog">
                                <div class="single-latest-blog-img">
                                    <a href="blog-details.html">
                                        <img src="{{asset('img/blog/1.jpg')}}" alt="">
                                    </a>
                                </div>
                                <div class="single-latest-blog-text">
                                    <div class="date-comment clearfix">
                                        <h4>Art, Business</h4>
                                        <h5>Jan 18, 2016 - Comments 5</h5>   
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-details.html">Lorem Ipsum Dammy Title</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodte porincidi ut labore et dolore magna aliqua. </p>   
                                    </div>
                                    <div class="continue-reading">
                                        <a href="blog-details.html">Continue Reading</a>
                                        <div class="blog-icon">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                            </ul>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="single-latest-blog">
                                <div class="single-latest-blog-img">
                                    <a href="blog-details.html">
                                        <img src="{{asset('img/blog/2.jpg')}}" alt="">
                                    </a>
                                </div>
                                <div class="single-latest-blog-text">
                                    <div class="date-comment clearfix">
                                        <h4>Art, Business</h4>
                                        <h5>Jan 18, 2016 - Comments 5</h5>   
                                    </div>
                                    <div class="blog-content">
                                        <h4><a href="blog-details.html">Lorem Ipsum Dammy Title</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodte porincidi ut labore et dolore magna aliqua. </p>   
                                    </div>
                                    <div class="continue-reading">
                                        <a href="blog-details.html">Continue Reading</a>
                                        <div class="blog-icon">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-share-alt"></i></a></li>
                                                <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
                                            </ul>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <!-- blog end -->
        <!-- client start -->
        <div class="client-area clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title">
                            <h2>popular Brand</h2>
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="client-owl">
                        <div class="col-md-12">   
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/1.png')}}" alt=""></a>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/2.png')}}" alt=""></a>
                            </div>
                        </div>  
                        <div class="col-md-12">  
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/3.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/4.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/5.png')}}" alt=""></a>
                            </div>
                        </div>  
                        <div class="col-md-12">  
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/1.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/2.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/3.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/4.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/5.png')}}" alt=""></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        <!-- client end --> 
@endsection