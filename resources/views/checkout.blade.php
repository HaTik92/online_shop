@extends('layouts.layout')
@section("title", "Products")
@section("content")
<!-- breadcrumb start -->
<div class="breadcrumb-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<ul class="breadcrumb">
					<li><a href="index.html">Home</a><span> - </span></li>
					<li class="active">checkout</li>
				</ul>
			</div>
		</div>
	</div>
</div> 
<!-- breadcrumb end -->
<!--Checkout Area Start-->
 <div class="checkout-area area-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h4 class="panel-title">
					<a href="#">
						ORDER REVIEW
					</a>
				</h4>
				<div class="panel-body checkout">
					<form action="#" method="post">
						<div class="checkout-table table-responsive">
							<table>
								<thead>
									<tr>
										<th class="p-name">Product</th>
										<th class="p-total">Total</th>
									</tr>
								</thead>
								<tbody>
									@foreach($carts as $cart)
									<tr>
										<td class="p-name">{{$cart['product']->name}}</td>
										<td class="p-total">${{$cart['product']->price}} x {{$cart['count']}}</td>
									</tr>
									@endforeach
									{{-- <tr>
										<td class="p-name">Lorem ipsum dolor sit amet X 1</td>
										<td class="p-total">$156.17</td>
									</tr> --}}
									<tr>
										<td class="p-name-subtotal">cart subtotal</td>
										<td class="p-total">${{$total}}</td>
									</tr>
									<tr>
										<td class="p-name-shipping">shipping</td>
										<td class="p-total">
											<label>
												<input type="radio" name="check" value="guest">
												<span>Flat rate $8.00</span>
											</label>
											<label>
												<input type="radio" name="check" value="user" checked="checked">
												<span>Free Shipping</span>
											</label>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td class="p-name-total">order total</td>
										<td>$160.17</td>
									</tr>
								</tfoot>
							</table>
							<button type="button" id="placeorder" class="button floatright"><span><a href="{{url('/pay/order')}}">Place Order</a></span></button>
						</div>
					</form>
				</div>
			</div>    
		</div>
	</div>
</div>
<!--End of Checkout Area--> 

@endsection