@extends('layouts.layout')
@section("title", "Wishlist")
@section("content")
<!-- breadcrumb start -->
<div class="breadcrumb-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<ul class="breadcrumb">
					<li><a href="index.html">Home</a><span> - </span></li>
					<li class="active">wishlist</li>
				</ul>
			</div>
		</div>
	</div>
</div> 
<!-- breadcrumb end -->
<!-- wishlist start -->
<div class="wishlist-area">
	<div class="container">          
		<div class="row">
			<div class="col-xs-12">   
				<div class="cart_list table-responsive">
					<table class="table_cart table-bordered">
						<thead>
							<tr>
								<th class="id">#</th>
								<th class="product">Image</th>
								<th class="description">Product Name</th>
								<th class="quantity">Stock Status</th>
								<th class="price">Price</th>
								<th class="cart">Add to cart</th>
								<th class="action">Remove</th>
							</tr>
						</thead>
						<tbody>
							@foreach($wishlist as $prod)
							<tr>
								<td class="id">1</td>
								<td class="product_img"><a href="#"><img alt="cart" src="{{asset('img/product/'.$prod['product']->address)}}"></a></td>
								<td class="product_des">
									<h3><a href="#">{{$prod['product']->name}}</a></h3>
								</td>
								<td class="p_quantity">
									<div class="stock">
										<a href="#">in stock</a>
									</div>
								</td>
								<td class="u_price">${{$prod['product']->price}}</td>
								<th class="add-cart" data-id="{{$prod['id']}}" data-prod_id="{{$prod['product_id']}}"><a href="{{url('/product/cart')}}">Add to cart</a></th>
								<td class="p_action delete_wishlist" data-id="{{$prod['id']}}">
									<a><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							@endforeach
							{{-- <tr>
								<td class="id">2</td>
								<td class="product_img"><a href="#"><img alt="cart" src="img/cart/2.jpg"></a></td>
								<td class="product_des">
									<h3><a href="#">Lorem ipsum dolor sit amet.</a></h3>
								</td>
								<td class="p_quantity">
									<div class="stock">
										<a href="#">in stock</a>
									</div>
								</td>
								<td class="u_price">$104.99</td>
								<th class="add-cart"><a href="{{url('/product/cart')}}">Add to cart</a></th>
								<td class="p_action">
									<a href="#"><i class="fa fa-trash"></i></a>
								</td>
							</tr> --}}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- wishlist end -->  
<script src="{{asset('js/cart.js')}}"></script>

@endsection