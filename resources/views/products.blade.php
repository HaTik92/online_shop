@extends('layouts.layout')
@section("title", "Products")
@section("content")
<div class="container">
	

	<!-- latest blog start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a><span> - </span></li>
                            <li><a href="shop.html">category</a><span> - </span></li>
                            <li class="active">shop grid with left sidebar</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>        
        <div class="shop-grid-leftsidebar-area">
            <div class="container">   
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="shop-left-sidebar">
                            <div class="single-left-widget">
                                <div class="section-title">
                                    <h4>category</h4>
                                    <ul>
                                        <li><a href="#" class="show-submenu">Women<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">casual print</a></li>
                                                <li><a href="#">lora dress</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">men<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">Coat</a></li>
                                                <li><a href="#">Shirt</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">Kid<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">flowral print</a></li>
                                                <li><a href="#">lora dress</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">Jewellery<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">Traditional</a></li>
                                                <li><a href="#">Classy</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">Glasses<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">casual</a></li>
                                                <li><a href="#">branded</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">Bags<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">casual</a></li>
                                                <li><a href="#">branded</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">Accessories<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">casual</a></li>
                                                <li><a href="#">branded</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#" class="show-submenu">Trends<i class="fa fa-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="#">casual</a></li>
                                                <li><a href="#">classy</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="single-left-widget">
                                <div class="section-title">
                                    <h4>filter by price</h4>
                                    <div class="price-filter">
                                        <div id="slider-range"></div>
                                        <div class="price-slider-amount">
                                           <div class="slider-values">
                                                <label>Range</label>
                                                <input type="text" id="amount" name="price"  placeholder="Add Your Price" /> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single-left-widget padding-none">
                                <div class="section-title">
                                    <h4>filter by size</h4>
                                    <ul class="size-widget">
                                        <li><a href="#">XS</a></li>
                                        <li><a href="#">S</a></li>
                                        <li><a href="#">M</a></li>
                                        <li><a href="#">L</a></li>
                                        <li><a href="#">XL</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="single-left-widget">
                                <div class="section-title">
                                    <h4>filter by color</h4>
                                    <ul class="color-widget">
                                        <li class="active"><span class="black"></span><a href="#">black</a></li>
                                        <li><span class="white"></span><a href="#">white</a></li>
                                        <li><span class="red"></span><a href="#">red</a></li>
                                        <li><span class="blue"></span><a href="#">blue</a></li>
                                        <li><span class="pink"></span><a href="#">pink</a></li>
                                        <li><span class="yellow"></span><a href="#">yellow</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="single-left-widget">
                                <div class="section-title">
                                    <h4>filter by brands</h4>
                                    <ul>
                                        <li><a href="#">rayban</a></li>
                                        <li><a href="#">denim</a></li>
                                        <li><a href="#">polo</a></li>
                                        <li><a href="#">adiddas</a></li>
                                        <li><a href="#">nike</a></li>
                                        <li><a href="#">yellow</a></li>
                                        <li><a href="#">arong</a></li>
                                        <li><a href="#">gucci</a></li>
                                        <li><a href="#">cats eye</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="single-left-widget without-background hidden-sm hidden-md">
                                <div class="section-title">
                                    <h4>hot deals</h4>
                                    <div class="widget-banner">
                                        <a href="#"><img src="{{asset('img/banner/8.jpg')}}" alt=""></a>
                                        <a href="shop-full-grid.html" class="shop-now">shop now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="col-md-9 col-sm-12 col-xs-12">   
                        <div class="shop-item-filter">
                            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                                <div class="shop-tab clearfix">
                                    <!-- Nav tabs -->
                                    <ul role="tablist">
                                        <li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="grid" class="grid-view" href="#grid"><i class="fa fa-th"></i></a></li>
                                        <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="list" class="list-view" href="#list"><i class="fa fa-th-list"></i></a></li>
                                    </ul>
                                </div>
                            </div>    
                            <div class="col-lg-4 col-md-5 col-sm-4 hidden-xs">      
                                <div class="filter-by text-center">
                                    <h4>Short by: </h4>
                                    <form action="#">
                                        <div class="select-filter">
                                            <select>
                                              <option value="color">Position</option>
                                              <option value="name">Name</option>
                                              <option value="brand">Brand</option>
                                            </select> 
                                        </div>
                                    </form>								
                                </div>
                            </div> 
                            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                                <div class="filter-by right">
                                    <h4>Show: </h4>
                                    <form action="#">
                                        <div class="select-filter">
                                            <select>
                                              <option value="10">12</option>
                                              <option value="20">16</option>
                                              <option value="30">20</option>
                                            </select> 
                                        </div>
                                    </form>	
                                </div>
                            </div>    
                        </div>
                        <div class="clearfix"></div>
                        <div class="tab-content">
                            <div id="grid" class="tab-pane active" role="tabpanel">
                            	{{-- <div class="row">
                            			
                            			<div class="col-lg-3 bg-dark p-4 text-light">
                            				<h1>{{$prod["name"]}}</h1>
                            				<h3>
                            					<a href="{{url('product/single/'.$prod['id'])}}">View Product</a>
                            				</h3>
                            				<img src="{{asset('img/product/'.$prod['photos'][0]['address'])}}" width="200" alt="">
                            			</div>
                            				
                            		</div> --}}
                            	<div class="row">
                            		@foreach($products as $prod)
                            		<div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="single-product">
                                            <div class="single-product-item">
                                                <div class="single-product-img clearfix hover-effect">
                                                    <a href="#">
                                                        <img class="primary-image" src="{{asset('img/product/'.$prod['photos'][0]['address'])}}" alt="">
                                                    </a>
                                                </div>
                                                <div class="single-product-info clearfix"> 
                                                    <div class="pro-price">
                                                        <span class="new-price">$25.99</span>
                                                    </div>
                                                    <div class="new-sale">
                                                        <span>new</span>
                                                    </div>  
                                                </div>
                                                <div class="product-content text-center">
                                                    <h3>{{$prod["name"]}}</h3>
                                                    <h4><a href="{{url('product/single/'.$prod['id'])}}">view details</a></h4>
                                                </div>
                                                <div class="product-action">      
                                                    <ul>
                                                        <li><a data-toggle="tooltip" title="Compage"><i class="fa fa-refresh"></i></a></li>
                                                        <li class="add-bag"><a data-toggle="tooltip" title="Shopping Cart">Add to Bag</a></li>
                                                        <li><a class="addwish" id="{{$prod->id}}" data-toggle="tooltip" title="Like it!"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  @endforeach
                            	</div>
                            </div>
                            <div id="list" class="tab-pane" role="tabpanel">
                                <div class="row">
                                    <div class="shop-product-list col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="single-product">
                                                    <div class="single-product-item">
                                                        <div class="single-product-img clearfix hover-effect">
                                                            <a href="#">
                                                                <img class="primary-image" src="{{asset('img/product/1.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="single-product-info clearfix"> 
                                                            <div class="new-sale">
                                                                <span>new</span>
                                                            </div>  
                                                        </div>
                                                        <div class="product-content text-center">
                                                            <h3>Full sleev women shirt</h3>
                                                            <h4><a href="#">view details</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="shop-product-text">
                                                    <h4><a href="#">Full sleev women T-shirt</a></h4>
                                                    <div class="price-rating-container">    
                                                        <div class="price-box"><span>$20.99</span> <del>($25.99)</del></div>
                                                        <div class="rating-right">
                                                            <div class="star-content">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability">AVAILABILITY: <span> In stock</span></div>
                                                    <h5 class="overview">overview:</h5>
                                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor indunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					                                </p>
                                                    <div class="shop-buttons">
                                                        <a class="cart-btn" href="cart.html"><span>Add to Bag</span></a>
                                                        <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                        <a href="#"><i class="fa fa-refresh"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shop-product-list col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="single-product">
                                                    <div class="single-product-item">
                                                        <div class="single-product-img clearfix hover-effect">
                                                            <a href="#">
                                                                <img class="primary-image" src="{{asset('img/product/10.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="single-product-info clearfix"> 
                                                            <div class="new-sale">
                                                                <span>new</span>
                                                            </div>  
                                                        </div>
                                                        <div class="product-content text-center">
                                                            <h3>Full sleev women shirt</h3>
                                                            <h4><a href="#">view details</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="shop-product-text">
                                                    <h4><a href="#">Full sleev women T-shirt</a></h4>
                                                    <div class="price-rating-container">    
                                                        <div class="price-box"><span>$20.99</span> <del>($25.99)</del></div>
                                                        <div class="rating-right">
                                                            <div class="star-content">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability">AVAILABILITY: <span> In stock</span></div>
                                                    <h5 class="overview">overview:</h5>
                                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor indunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					                                </p>
                                                    <div class="shop-buttons">
                                                        <a class="cart-btn" href="cart.html"><span>Add to Bag</span></a>
                                                        <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                        <a href="#"><i class="fa fa-refresh"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shop-product-list col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="single-product">
                                                    <div class="single-product-item">
                                                        <div class="single-product-img clearfix hover-effect">
                                                            <a href="#">
                                                                <img class="primary-image" src="{{asset('img/product/4.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="single-product-info clearfix"> 
                                                            <div class="new-sale">
                                                                <span>new</span>
                                                            </div>  
                                                        </div>
                                                        <div class="product-content text-center">
                                                            <h3>Full sleev women shirt</h3>
                                                            <h4><a href="#">view details</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="shop-product-text">
                                                    <h4><a href="#">Full sleev women T-shirt</a></h4>
                                                    <div class="price-rating-container">    
                                                        <div class="price-box"><span>$20.99</span> <del>($25.99)</del></div>
                                                        <div class="rating-right">
                                                            <div class="star-content">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability">AVAILABILITY: <span> In stock</span></div>
                                                    <h5 class="overview">overview:</h5>
                                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor indunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					                                </p>
                                                    <div class="shop-buttons">
                                                        <a class="cart-btn" href="cart.html"><span>Add to Bag</span></a>
                                                        <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                        <a href="#"><i class="fa fa-refresh"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shop-product-list col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="single-product">
                                                    <div class="single-product-item">
                                                        <div class="single-product-img clearfix hover-effect">
                                                            <a href="#">
                                                                <img class="primary-image" src="{{asset('img/product/7.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="single-product-info clearfix"> 
                                                            <div class="new-sale">
                                                                <span>new</span>
                                                            </div>  
                                                        </div>
                                                        <div class="product-content text-center">
                                                            <h3>Full sleev women shirt</h3>
                                                            <h4><a href="#">view details</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="shop-product-text">
                                                    <h4><a href="#">Full sleev women T-shirt</a></h4>
                                                    <div class="price-rating-container">    
                                                        <div class="price-box"><span>$20.99</span> <del>($25.99)</del></div>
                                                        <div class="rating-right">
                                                            <div class="star-content">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability">AVAILABILITY: <span> In stock</span></div>
                                                    <h5 class="overview">overview:</h5>
                                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor indunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					                                </p>
                                                    <div class="shop-buttons">
                                                        <a class="cart-btn" href="cart.html"><span>Add to Bag</span></a>
                                                        <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                        <a href="#"><i class="fa fa-refresh"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shop-product-list col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="single-product">
                                                    <div class="single-product-item">
                                                        <div class="single-product-img clearfix hover-effect">
                                                            <a href="#">
                                                                <img class="primary-image" src="{{asset('img/product/13.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="single-product-info clearfix"> 
                                                            <div class="new-sale">
                                                                <span>new</span>
                                                            </div>  
                                                        </div>
                                                        <div class="product-content text-center">
                                                            <h3>Full sleev women shirt</h3>
                                                            <h4><a href="#">view details</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="shop-product-text">
                                                    <h4><a href="#">Full sleev women T-shirt</a></h4>
                                                    <div class="price-rating-container">    
                                                        <div class="price-box"><span>$20.99</span> <del>($25.99)</del></div>
                                                        <div class="rating-right">
                                                            <div class="star-content">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability">AVAILABILITY: <span> In stock</span></div>
                                                    <h5 class="overview">overview:</h5>
                                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor indunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					                                </p>
                                                    <div class="shop-buttons">
                                                        <a class="cart-btn" href="cart.html"><span>Add to Bag</span></a>
                                                        <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                        <a href="#"><i class="fa fa-refresh"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="shop-product-list col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="single-product">
                                                    <div class="single-product-item">
                                                        <div class="single-product-img clearfix hover-effect">
                                                            <a href="#">
                                                                <img class="primary-image" src="{{asset('img/product/2.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <div class="single-product-info clearfix"> 
                                                            <div class="new-sale">
                                                                <span>new</span>
                                                            </div>  
                                                        </div>
                                                        <div class="product-content text-center">
                                                            <h3>Full sleev women shirt</h3>
                                                            <h4><a href="#">view details</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="shop-product-text">
                                                    <h4><a href="#">Full sleev women T-shirt</a></h4>
                                                    <div class="price-rating-container">    
                                                        <div class="price-box"><span>$20.99</span> <del>($25.99)</del></div>
                                                        <div class="rating-right">
                                                            <div class="star-content">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability">AVAILABILITY: <span> In stock</span></div>
                                                    <h5 class="overview">overview:</h5>
                                                    <p class="product-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor indunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
					                                </p>
                                                    <div class="shop-buttons">
                                                        <a class="cart-btn" href="cart.html"><span>Add to Bag</span></a>
                                                        <a href="wishlist.html"><i class="fa fa-heart-o"></i></a>
                                                        <a href="#"><i class="fa fa-refresh"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">   
                                <div class="shop-item-filter bottom">
                                    <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">
                                        <div class="shop-tab clearfix">
                                            <!-- Nav tabs -->
                                            <ul role="tablist">
                                                <li role="presentation" class="active"><a data-toggle="tab" role="tab" aria-controls="grid" class="grid-view" href="#grid"><i class="fa fa-th"></i></a></li>
                                                <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="list" class="list-view" href="#list"><i class="fa fa-th-list"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>    
                                    <div class="col-lg-4 col-md-5 col-sm-4 hidden-xs">      
                                        <div class="filter-by text-center">
                                            <h4>Short by: </h4>
                                            <form action="#">
                                                <div class="select-filter">
                                                    <select>
                                                      <option value="color">Position</option>
                                                      <option value="name">Name</option>
                                                      <option value="brand">Brand</option>
                                                    </select> 
                                                </div>
                                            </form>								
                                        </div>
                                    </div> 
                                    <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                                        <div class="filter-by right">
                                            <h4>Show: </h4>
                                            <form action="#">
                                                <div class="select-filter">
                                                    <select>
                                                      <option value="10">12</option>
                                                      <option value="20">16</option>
                                                      <option value="30">20</option>
                                                    </select> 
                                                </div>
                                            </form>	
                                        </div>
                                    </div>    
                                </div>
                            </div>    
                        </div> 
                    </div>   
                </div>
            </div>
        </div>
        <!-- latest blog end -->
        <!-- client start -->
        <div class="client-area clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-title">
                            <h2>popular Brand</h2>
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="client-owl">
                        <div class="col-md-12">   
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/1.png')}}" alt=""></a>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/2.png')}}" alt=""></a>
                            </div>
                        </div>  
                        <div class="col-md-12">  
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/3.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/4.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/5.png')}}" alt=""></a>
                            </div>
                        </div>  
                        <div class="col-md-12">  
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/1.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/2.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/3.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/4.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-12">    
                            <div class="single-client">
                                <a href="#"><img src="{{asset('img/client/5.png')}}" alt=""></a>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        <!-- client end --> 
</div>
<script src="{{asset('js/wishlist.js')}}"></script>

@endsection