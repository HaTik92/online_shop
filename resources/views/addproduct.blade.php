@extends('layouts.layout')
@section("title", "Add product")
@section("content")
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> --}}

<div class="container">
	<form class="form w-50" action="{{url('/product/add/product')}}" method="post" enctype="multipart/form-data">
		@csrf
		<input type="text" name="name" class="form-control" placeholder="Product name">
		<input type="text" name="count" class="form-control" placeholder="Count">
		<input type="text" name="price" class="form-control" placeholder="Price">
		<input type="text" name="description" class="form-control" placeholder="Description">
		<input type="file" name="photo[]" class="form-control" multiple>
		<select class="form-control" name="category" id="">
			@for($i=0; $i < $cat->count(); $i++)
			<option value="{{$cat[$i]['id']}}">
				{{$cat[$i]['name']}}
			</option>
			@endfor
		</select>
		<button class="btn btn-info">Save</button>
	</form>
</div>
@endsection