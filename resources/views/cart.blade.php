@extends('layouts.layout')
@section("title", "Cart")
@section("content")
<!-- breadcrumb start -->
<div class="breadcrumb-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-left">
				<ul class="breadcrumb">
					<li><a href="index.html">Home</a><span> - </span></li>
					<li class="active">shopping cart</li>
				</ul>
			</div>
		</div>
	</div>
</div> 
<!-- breadcrumb end -->
<!-- cart start -->
<div class="cart-area">
	<div class="container">          
		<div class="row">
			<div class="col-xs-12">   
				<div class="cart_list table-responsive">
					<table class="table_cart table-bordered">
						<thead>
							<tr>
								<th class="id">#</th>
								<th class="product">Image</th>
								<th class="description">Product Name</th>
								<th class="quantity">Quantity</th>
								<th class="price">Price</th>
								<th class="value">Total</th>
								<th class="action">Remove</th>
							</tr>
						</thead>
						<tbody>
							@foreach($cart as $prod)
							<tr>
								<td class="id">1</td>
								<td class="product_img"><a href="#"><img alt="cart" src="{{asset('img/product/'.$prod['product']->address)}}"></a></td>
								<td class="product_des">
									<h3><a href="#">{{$prod['product']->name}}</a></h3>
								</td>
								<td class="p_quantity">
									{{-- <div class="pp_quantity">
										<select class="category-items" name="category">
											<option>01</option>
											<option>02</option>
											<option>03</option>
											<option>04</option>
											<option>05</option>
										</select>
									</div> --}}
									<div class="quantity">
										<button class="plus-btn" type="button" name="button">
											+
										</button>
										<input type="text" name="name" value="1">
										<button class="minus-btn" type="button" name="button">
											-
										</button>
									</div>
								</td>
								<td class="u_price">${{$prod['product']->price}}</td>
								<td class="p_value">$104.99</td>
								<td class="p_action delete_cart" data-id="{{$prod['id']}}">
									<a><i class="fa fa-trash"></i></a>
								</td>
							</tr>
							@endforeach
							{{-- <tr>
								<td class="id">2</td>
								<td class="product_img"><a href="#"><img alt="cart" src="img/cart/2.jpg"></a></td>
								<td class="product_des">
									<h3><a href="#">Lorem ipsum dolor sit amet.</a></h3>
								</td>
								<td class="p_quantity">
									<div class="pp_quantity">
										<select class="category-items" name="category">
											<option>01</option>
											<option>02</option>
											<option>03</option>
											<option>04</option>
											<option>05</option>
										</select>
									</div>
								</td>
								<td class="u_price">$104.99</td>
								<td class="p_value">$104.99</td>
								<td class="p_action">
									<a href="#"><i class="fa fa-trash"></i></a>
								</td>
							</tr> --}}
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5 col-md-4 col-sm-12">           
				<a href="shop-full-grid.html" class="continue-shopping">continue shopping</a>
				<a href="shop-full-grid.html" class="continue-shopping">update shopping bag</a>
			</div>    
			<div class="col-lg-7 col-md-8 col-sm-12">      
				<div class="discount-code">
					<h2>discount codes</h2>
					<p>Enter your coupon code if you have one.</p>
					<input type="text">
					<input type="submit" value="apply coupon">
				</div>
				<div class="total text-right">
					<h2>subtotal <span>$190.98</span></h2>
					<h2 class="strong">grandtotal <span>$190.98</span></h2>
					<input class="process-checkout" type="submit" value="process to checkout">
				</div>
			</div>    
		</div>
	</div>
</div> 
<!-- cart end -->  
<script src="{{asset('js/cart.js')}}"></script>

@endsection