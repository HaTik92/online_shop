<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<title>Document</title>
	<link rel="stylesheet" href="{{asset('css/register.css')}}">
	
</head>
<body>
	<div class="container">
		<form action="{{url('/login')}}" method="post">
			@csrf
			<label for="email">{{$errors->first("email")}}</label>
			<input type="text" value="{{old('email')}}" name="email" class="form-control" placeholder="Enter e-mail">
			<label for="password">{{$errors->first("password")}}</label>
			<input type="password" name="password" class="form-control" placeholder="Enter password">
			{{-- <input type="text" name="email" class="form-control">
			<input type="password" name="password" class="form-control"> --}}
			<button class="btn btn-info">Login</button>

		</form>
	</div>
</body>
</html>